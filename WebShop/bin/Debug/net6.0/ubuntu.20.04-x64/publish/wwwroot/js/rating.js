﻿const ratingInput = document.getElementById('ratingInput');
const ratingStars = [...document.getElementsByClassName("rating__star")];

const starClassActive = "rating__star fas fa-star";
const starClassInactive = "rating__star far fa-star";
const starsLength = ratingStars.length;

let inputValue = ratingInput.value;

subscribeRating(ratingStars);
fillRating(ratingStars, inputValue);


function subscribeRating(stars) {
    let i;
    stars.map((star) => {
        star.onclick = () => {
            i = stars.indexOf(star);
            ratingInput.value = i;

            if (star.className===starClassInactive) {
                for (i; i >= 0; --i) stars[i].className = starClassActive;
            } else {
                for (i; i < starsLength; ++i) stars[i].className = starClassInactive;
            }
        };
    });
}

function fillRating(stars, i) {
    i--;
    if (i > starsLength) i = starsLength - 1;
    if (i < 0) i = 0;
    
    let star = stars[i];
    
    if (star.className===starClassInactive) {
        for (i; i >= 0; --i) stars[i].className = starClassActive;
    } else {
        for (i; i < starsLength; ++i) stars[i].className = starClassInactive;
    }
}