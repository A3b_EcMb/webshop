﻿const starClassActive = "rating__star fas fa-star";
const starClassInactive = "rating__star far fa-star";

REVIEWS_setUpAllRatings();


function REVIEWS_setUpAllRatings() {
    let list = document.getElementsByClassName('rating-stars');
    for (let i = 0; i < list.length; i++) {
        REVIEWS_setUpRating(list[i]);
    }
}

function REVIEWS_setUpRating(root) {
    let ratingInput = root.getElementsByClassName('ratingInput')[0];
    let i = ratingInput.value;

    let ratingStars = [...root.getElementsByClassName("rating__star")];
    fillRating(ratingStars, i);
}

function fillRating(stars, i) {
    i--;
    if (i > stars.length) i = stars.length - 1;
    if (i < 0) i = 0;

    let star = stars[i];

    if (star.className===starClassInactive) {
        for (i; i >= 0; --i) stars[i].className = starClassActive;
    } else {
        for (i; i < stars.length; ++i) stars[i].className = starClassInactive;
    }
}