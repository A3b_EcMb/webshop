﻿let mainRoot = document.getElementById('grid');
let modalRoot = document.getElementById('sockModalContent');
let inputField = document.getElementById('input-search-name');
let searchButton = document.getElementById('btn-search');
searchButton.addEventListener('click', () => {
    FILTER_updateList();
});

FILTER_updateList();

function FILTER_updateList() {
    let input = inputField.value;
    FILTER_loadSocks(mainRoot, input);
}

function FILTER_loadSocks(root, input) {
    fetch('/Catalog/FilterByName?input=' + input)
        .then((response) => {
            return response.text();
        })
        .then((result) => {
            root.innerHTML = result;
            FILTER_setUpAllModals(root);
        });
}

function FILTER_setUpAllModals(root){
    let list = root.getElementsByClassName('sock-panel');
    for (let i = 0; i < list.length; i++){
        FILTER_setUpModalButton(list[i], modalRoot);
    }
}

function FILTER_setUpModalButton(root, modalRoot){
    let button = root.getElementsByTagName('button')[0];
    if (button == null) {
        // console.log("button is null");
        return;
    }
    // console.log("button not null");

    let input = root.getElementsByTagName('input')[0];
    if (input == null){
        // console.log("input is null");
        return;
    }
    // console.log("input not null");
    let id = input.value;

    button.addEventListener('click', () => {
        console.log("opening " + id + " sock modal window");
        fetch("/Sock/GetSockModal?id=" + id)
            .then((response) => {
                return response.text();
            })
            .then((result) => {
                modalRoot.innerHTML = result;
                console.log("loaded sock modal")
                let cartRoot = modalRoot.getElementsByClassName('cart-controls')[0];
                if (cartRoot == null) {
                    console.log("cart root is null");
                    return;
                }
                FILTER_loadCartInfo(cartRoot);
            });
    });
}

function FILTER_loadCartInfo(root) {
    let input = root.getElementsByTagName('input')[0];
    if (input == null){
        console.log("input is null");
        return;
    }
    console.log("input not null");

    let id = input.value;
    console.log("input value is " + id);
    fetch('/Cart/GetCartInfo?id='+id)
        .then((response) => {
            return response.text();
        })
        .then((result) => {
            root.innerHTML = result;
            FILTER_setUpButtons(root, id);
        });
}

function FILTER_setUpButtons(root, id) {
    let addBtn = root.getElementsByTagName('input')[0];
    if (addBtn != null) {
        console.log("binding add button");
        addBtn.addEventListener('click', () => {
            console.log("adding " + id + " to cart");
            fetch('/Cart/GetCartInfo?id=' + id + '&change=' + 1)
                .then((response) => {
                    return response.text();
                })
                .then((result) => {
                    root.innerHTML = result;
                    FILTER_setUpButtons(root, id);
                });
        })
    }
    else {
        console.log(id + "add button is null");
    }

    let removeBtn = root.getElementsByTagName('input')[1];
    if (removeBtn != null)  {
        console.log("binding remove button");
        removeBtn.addEventListener('click', () => {
            console.log("removing " + id + " from cart");
            fetch('/Cart/GetCartInfo?id='+id+'&change='+'-1')
                .then((response) => {
                    return response.text();
                })
                .then((result) => {
                    root.innerHTML = result;
                    FILTER_setUpButtons(root, id);
                });
        })
    }
    else {
        console.log(id + "remove button is null");
    }
}