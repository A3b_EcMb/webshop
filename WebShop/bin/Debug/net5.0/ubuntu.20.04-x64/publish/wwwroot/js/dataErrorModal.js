﻿const isVisible = "is-visible";
let hasInputError = document.getElementById("was-input-error");

if (hasInputError != null) {
    let inputErrorModal = document.getElementById("inputErrorModal");
    if (inputErrorModal != null) {
        inputErrorModal.modal('show');
    }
}