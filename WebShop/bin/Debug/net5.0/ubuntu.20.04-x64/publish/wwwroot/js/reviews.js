﻿const starClassActive = "rating__star fas fa-star";
const starClassInactive = "rating__star far fa-star";

let reviewModalRoot = document.getElementById('reviewModalContent');

REVIEWS_setUpAllRatings();
REVIEWS_setUpAllModals(reviewModalRoot);

function REVIEWS_setUpAllRatings() {
    let list = document.getElementsByClassName('rating-stars');
    for (let i = 0; i < list.length; i++) {
        REVIEWS_setUpRating(list[i]);
    }
}

function REVIEWS_setUpRating(root) {
    let ratingInput = root.getElementsByClassName('ratingInput')[0];
    let i = ratingInput.value;

    let ratingStars = [...root.getElementsByClassName("rating__star")];
    fillRating(ratingStars, i);
}

function fillRating(stars, i) {
    i--;
    if (i > stars.length) i = stars.length - 1;
    if (i < 0) i = 0;

    let star = stars[i];

    if (star.className===starClassInactive) {
        for (i; i >= 0; --i) stars[i].className = starClassActive;
    } else {
        for (i; i < stars.length; ++i) stars[i].className = starClassInactive;
    }
}

function REVIEWS_setUpAllModals(modalRoot){
    let list = document.getElementsByClassName('review-panel');
    console.log("setting " + list.length + " modal buttons");
    for (let i = 0; i < list.length; i++){
        REVIEWS_setUpModalButton(list[i], modalRoot);
    }
}

function REVIEWS_setUpModalButton(root, modalRoot){
    let button = root.getElementsByTagName('button')[0];
    if (button == null) {
        console.log("button is null");
        return;
    }
    console.log("button not null");

    let input = root.getElementsByTagName('input')[0];
    if (input == null){
        console.log("input is null");
        return;
    }
    console.log("input not null");
    let id = input.value;

    button.addEventListener('click', () => {
        console.log("opening " + id + " review modal window");
        fetch("/Reviews/GetReviewModal?id=" + id)
            .then((response) => {
                return response.text();
            })
            .then((result) => {
                modalRoot.innerHTML = result;
                console.log("loaded review modal");
            });
    });
}