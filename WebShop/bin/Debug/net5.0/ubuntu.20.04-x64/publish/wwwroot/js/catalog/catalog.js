﻿let root = document.getElementById('grid');
let modalRoot = document.getElementById('sockModalContent');

CATALOG_updateList(root, modalRoot);

function CATALOG_updateList() {
    CATALOG_loadSocks(root, modalRoot);
}

function CATALOG_loadSocks(root, modalRoot){
    fetch('/Catalog/GetSocks')
        .then((response) => {
            return response.text();
        })
        .then((result) => {
            root.innerHTML = result;
            CATALOG_setUpAllModals(root, modalRoot);
        });
}

function CATALOG_setUpAllModals(root){
    let list = root.getElementsByClassName('sock-panel');    
    for (let i = 0; i < list.length; i++){
        CATALOG_setUpModalButton(list[i], modalRoot);
    }
}

function CATALOG_setUpModalButton(root, modalRoot){
    let button = root.getElementsByTagName('button')[0];
    if (button == null) {
        // console.log("button is null");
        return;
    }
    // console.log("button not null");

    let input = root.getElementsByTagName('input')[0];
    if (input == null){
        // console.log("input is null");
        return;
    }
    // console.log("input not null");
    let id = input.value;

    button.addEventListener('click', () => {
        console.log("opening " + id + " sock modal window");
        fetch("/Sock/GetSockModal?id=" + id)
            .then((response) => {
                return response.text();
            })
            .then((result) => {
                modalRoot.innerHTML = result;
                // console.log("loaded sock modal")
                let cartRoot = modalRoot.getElementsByClassName('cart-controls')[0];
                if (cartRoot == null) {
                    // console.log("cart root is null");
                    return;
                }
                CATALOG_loadCartInfo(cartRoot);
            });
    });
}

function CATALOG_loadCartInfo(root) {
    let input = root.getElementsByTagName('input')[0];
    if (input == null){
        console.log("input is null");
        return;
    }
    console.log("input not null");
    
    let id = input.value;
    console.log("input value is " + id);
    fetch('/Cart/GetCartInfoCatalog?id='+id)
        .then((response) => {
            return response.text();
        })
        .then((result) => {
            root.innerHTML = result;
            CATALOG_setUpButtons(root, id);
        });
}

function CATALOG_setUpButtons(root, id) {    
    let addBtn = root.getElementsByTagName('input')[0];
    if (addBtn != null) {
        console.log("binding add button");
        addBtn.addEventListener('click', () => {
            console.log("adding " + id + " to cart");
            fetch('/Cart/GetCartInfoCatalog?id=' + id + '&change=' + 1)
                .then((response) => {
                    return response.text();
                })
                .then((result) => {
                    root.innerHTML = result;
                    CATALOG_setUpButtons(root, id);
                });
        })
    }
    else {
        console.log(id + "add button is null");
    }

    let removeBtn = root.getElementsByTagName('input')[1];
    if (removeBtn != null)  {
        console.log("binding remove button");
        removeBtn.addEventListener('click', () => {
            console.log("removing " + id + " from cart");
            fetch('/Cart/GetCartInfoCatalog?id='+id+'&change='+'-1')
                .then((response) => {
                    return response.text();
                })
                .then((result) => {
                    root.innerHTML = result;
                    CATALOG_setUpButtons(root, id);
                });
        })
    }
    else {
        console.log(id + "remove button is null");
    }
}