﻿const starClassActive = "rating__star fas fa-star";
const starClassInactive = "rating__star far fa-star";

function INDEX_loadReviews(root, modalRoot){
    fetch('/Reviews/GetReviewsIndex/')
        .then((response) => {
            return response.text();
        })
        .then((result) => {
            root.innerHTML = result;
            INDEX_setUpAllModalsReviews(reviewsModalRoot);
            INDEX_setUpAllRatings(root);
        });
}

function INDEX_setUpAllRatings(root) {
    let list = root.getElementsByClassName('rating-stars');
    for (let i = 0; i < list.length; i++) {
        INDEX_setUpRating(list[i]);
    }
}

function INDEX_setUpRating(root) {
    let ratingInput = root.getElementsByClassName('ratingInput')[0];
    let i = ratingInput.value;
    
    let ratingStars = [...root.getElementsByClassName("rating__star")];
    fillRating(ratingStars, i);
}

function fillRating(stars, i) {
    i--;
    if (i > stars.length) i = stars.length - 1;
    if (i < 0) i = 0;
    
    let star = stars[i];

    if (star.className===starClassInactive) {
        for (i; i >= 0; --i) stars[i].className = starClassActive;
    } else {
        for (i; i < stars.length; ++i) stars[i].className = starClassInactive;
    }
}

function INDEX_setUpAllModalsReviews(modalRoot){
    let list = document.getElementsByClassName('review-panel');
    console.log("[REVIEW] setting " + list.length + " modal buttons");
    for (let i = 0; i < list.length; i++){
        INDEX_setUpModalButtonReview(list[i], modalRoot);
    }
}

function INDEX_setUpModalButtonReview(root, modalRoot){
    let button = root.getElementsByTagName('button')[0];
    if (button == null) {
        console.log("[REVIEW] button is null");
        return;
    }
    console.log("[REVIEW] button not null");

    let input = root.getElementsByTagName('input')[0];
    if (input == null){
        console.log("[REVIEW] input is null");
        return;
    }
    console.log("[REVIEW] input not null");
    let id = input.value;

    button.addEventListener('click', () => {
        console.log("[REVIEW] opening " + id + " review modal window");
        fetch("/Reviews/GetReviewModal?id=" + id)
            .then((response) => {
                return response.text();
            })
            .then((result) => {
                modalRoot.innerHTML = result;
                console.log("[REVIEW] loaded review modal");
            });
    });
}