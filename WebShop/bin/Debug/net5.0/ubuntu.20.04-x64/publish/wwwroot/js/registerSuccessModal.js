﻿let hasRegisterSuccess = document.getElementById("has-register-success");

if (hasRegisterSuccess != null) {
    let registerSuccessModal = document.getElementById("registerSuccessModal");
    if (registerSuccessModal != null) {
        registerSuccessModal.style.display = "block";
    }
}