﻿let mainRoot = document.getElementById('grid');
let cartInfoRoot = document.getElementById('cart-info');
CART_loadSocksList(mainRoot);

function CART_loadCartInfo() {
    fetch('/Cart/GetTotalCartInfo/')
        .then((response) => response.text())
        .then((result) => {
            cartInfoRoot.innerHTML = result;
        })
}

function CART_loadSocksList(root) {
    console.log("loading socks list");
    root.innerHTML = "";
    fetch('/Cart/GetSocks')
        .then((response) => {
            console.log("got socks list response");
            return response.text();
        })
        .then((result) => {
            root.innerHTML = result;
            console.log("loaded socks list");
            CART_loadAllCarts(root);
        });
    CART_loadCartInfo();
}

function CART_loadAllCarts(root) {
    console.log("loading carts for socks");
    let list = root.getElementsByClassName('sock-card');
    for (let i = 0; i < list.length; i++){
        let sockRoot = list[i];
        let cartRoot = list[i].getElementsByClassName('cart-controls')[0];
        console.log("got cart " + i + " roots");
        CART_loadCartInfoForSock(sockRoot, cartRoot);
    }
}

function CART_loadCartInfoForSock(sockRoot, cartRoot) {
    console.log("loading cart info for sock");
    let input = cartRoot.getElementsByTagName('input')[0];
    if (input == null) return;

    let id = input.value;
    console.log("input value is " + id);
    fetch('/Cart/GetCartInfoInCart?id='+id)
        .then((response) => {
            return response.text();
        })
        .then((result) => {
            cartRoot.innerHTML = result;
            console.log("loaded cart info");
            CART_checkExistingAndLoadCartInfo(sockRoot, cartRoot, id);
        });
}

function CART_checkExistingAndLoadCartInfo(sockRoot, cartRoot, id) {
    console.log("item" + id + ": start checking existing in cart")
    fetch('/Cart/GetCartCount?id=' + id)
        .then((response) => {
            return response.text()
        })
        .then((result) => {
            console.log("item" + id + ": got existing info: " + result);
            if (parseInt(result) === 0) {
                console.log("item" + id + ": item not exists, clearing");
                CART_loadSocksList(mainRoot);
            }
            else {
                console.log("item" + id + ": item exists");
                CART_setUpButtons(sockRoot, cartRoot, id);
            }
        })
}

function CART_setUpButtons(sockRoot, cartRoot, id) {
    console.log("item" + id + ": setting callbacks to cart buttons");
    let addBtn = cartRoot.getElementsByTagName('input')[0];
    if (addBtn != null) {
        console.log("item" + id + ": binding add button");
        addBtn.addEventListener('click', () => {
            console.log("item" + id + ": adding to cart");
            fetch('/Cart/GetCartInfoInCart?id=' + id + '&change=' + 1)
                .then((response) => {
                    return response.text();
                })
                .then((result) => {
                    console.log("item" + id + ": received answer for add item");
                    cartRoot.innerHTML = result;
                    CART_checkExistingAndLoadCartInfo(sockRoot, cartRoot, id);
                    CART_loadCartInfo();
                });
        })
    }
    else {
        console.log(id + " add button is null");
    }

    let removeBtn = cartRoot.getElementsByTagName('input')[1];
    if (removeBtn != null)  {
        console.log("item" + id + ": binding remove button");
        removeBtn.addEventListener('click', () => {
            console.log("item" + id + ": removing from cart");
            fetch('/Cart/GetCartInfoInCart?id='+id+'&change='+'-1')
                .then((response) => {
                    return response.text();
                })
                .then((result) => {
                    console.log("item" + id + ": received answer for remove item");
                    cartRoot.innerHTML = result;
                    CART_checkExistingAndLoadCartInfo(cartRoot, id);
                    CART_loadCartInfo();
                });
        })
    }
    else {
        console.log(id + " remove button is null");
    }
}