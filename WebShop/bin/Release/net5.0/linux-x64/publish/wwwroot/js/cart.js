﻿let root = document.getElementById('grid');
loadSocks(root);

function loadSocks(root){
    fetch('/Catalog/GetSocks')
        .then((response) => {
            return response.text();
        })
        .then((result) => {
            root.innerHTML = result;
            loadAllCarts(root);
        });
}

function loadAllCarts(root){
    let list = root.getElementsByClassName('cart-controls');    
    for (let i = 0; i < list.length; i++){
        loadCartInfo(list[i]);
    }
}

function loadCartInfo(root) {
    let input = root.getElementsByTagName('input')[0];
    if (input == null){
        console.log("input is null");
        return;
    }
    console.log("input not null");
    
    let id = input.value;
    console.log("input value is " + id);
    fetch('/Catalog/GetCartInfo?id='+id)
        .then((response) => {
            return response.text();
        })
        .then((result) => {
            root.innerHTML = result;
            setUpButtons(root, id);
        });
}

function setUpButtons(root, id) {    
    let addBtn = root.getElementsByTagName('input')[0];
    if (addBtn != null) {
        console.log("binding add button");
        addBtn.addEventListener('click', () => {
            console.log("adding " + id + " to cart");
            fetch('/Catalog/GetCartInfo?id=' + id + '&change=' + 1)
                .then((response) => {
                    return response.text();
                })
                .then((result) => {
                    root.innerHTML = result;
                    setUpButtons(root, id);
                });
        })
    }
    else {
        console.log(id + "add button is null");
    }

    let removeBtn = root.getElementsByTagName('input')[1];
    if (removeBtn != null)  {
        console.log("binding remove button");
        removeBtn.addEventListener('click', () => {
            console.log("removing " + id + " from cart");
            fetch('/Catalog/GetCartInfo?id='+id+'&change='+'-1')
                .then((response) => {
                    return response.text();
                })
                .then((result) => {
                    root.innerHTML = result;
                    setUpButtons(root, id);
                });
        })
    }
    else {
        console.log(id + "remove button is null");
    }
}