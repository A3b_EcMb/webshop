﻿let data = document.getElementById("cartSpan");
data.style.display = "none";

setInterval(updateCartCount, 1000);
updateCartCount();

function updateCartCount() {
    let data = document.getElementById("cartSpan");
    if (data == null)
        return;
    
    fetch("/Cart/GetCartTotalCount")
        .then((response) => {
            return response.text();
        })
        .then((result) => {
            console.log("received cart count: " + result);
            if (result === "0") {
                console.log("hiding cart span");
                data.style.display = "none";
            }
            else {
                console.log("showing cart span");
                data.style.display = "inline";
                data.innerText = result;
            }
        });
}