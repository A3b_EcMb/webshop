﻿function INDEX_loadSocks(root, modalRoot){
    console.log("loading socks");
    fetch('/Catalog/GetSocksIndex')
        .then((response) => {
            return response.text();
        })
        .then((result) => {
            console.log("received socks: " + result);
            root.innerHTML = result;
            INDEX_setUpAllModalsSocks(root, modalRoot);
        });
}

function INDEX_setUpAllModalsSocks(root, modalRoot){
    let list = root.getElementsByClassName('sock-panel');
    console.log("[SOCK] setting " + list.length + " modal buttons");
    for (let i = 0; i < list.length; i++){
        INDEX_setUpModalButtonSock(list[i], modalRoot);
    }
}

function INDEX_setUpModalButtonSock(root, modalRoot){
    let button = root.getElementsByTagName('button')[0];
    if (button == null) {
        console.log("[SOCK] button is null");
        return;
    }
    console.log("[SOCK] button not null");
    
    let input = root.getElementsByTagName('input')[0];
    if (input == null){
        console.log("[SOCK] input is null");
        return;
    }
    console.log("[SOCK] input not null");
    let id = input.value;
    
    button.addEventListener('click', () => {
        console.log("[SOCK] opening " + id + " sock modal window");
        fetch("/Sock/GetSockModal?id=" + id)
            .then((response) => {
                return response.text();
            })
            .then((result) => {
                modalRoot.innerHTML = result;
                console.log("[SOCK] loaded sock modal")
                let cartRoot = modalRoot.getElementsByClassName('cart-controls')[0];
                if (cartRoot == null) {
                    console.log("[SOCK] cart root is null");
                    return;
                }
                INDEX_loadCartInfo(cartRoot);
            });
    });
}

function INDEX_loadCartInfo(root) {
    let input = root.getElementsByTagName('input')[0];
    if (input == null){
        // console.log("input is null");
        return;
    }
    // console.log("input not null");

    let id = input.value;
    // console.log("input value is " + id);
    fetch('/Cart/GetCartInfoHome?id='+id)
        .then((response) => {
            return response.text();
        })
        .then((result) => {
            root.innerHTML = result;
            INDEX_setUpButtons(root, id);
        });
}

function INDEX_setUpButtons(root, id) {
    let addBtn = root.getElementsByClassName('cart-add-btn')[0];
    if (addBtn != null) {
        // console.log("binding add button");
        addBtn.addEventListener('click', () => {
            // console.log("adding " + id + " to cart");
            fetch('/Cart/GetCartInfoHome?id=' + id + '&change=' + 1)
                .then((response) => {
                    return response.text();
                })
                .then((result) => {
                    root.innerHTML = result;
                    INDEX_setUpButtons(root, id);
                });
        });
    }
    else {
        // console.log(id + "add button is null");
    }

    let removeBtn = root.getElementsByClassName('cart-remove-btn')[0];
    if (removeBtn != null)  {
        // console.log("binding remove button");
        removeBtn.addEventListener('click', () => {
            // console.log("removing " + id + " from cart");
            fetch('/Cart/GetCartInfoHome?id='+id+'&change='+'-1')
                .then((response) => {
                    return response.text();
                })
                .then((result) => {
                    root.innerHTML = result;
                    INDEX_setUpButtons(root, id);
                });
        });
    }
    else {
        // console.log(id + "remove button is null");
    }
}