﻿const ratingInput = document.getElementById('ratingInput');
const ratingStars = [...document.getElementsByClassName("rating__star")];

const starClassActive = "rating__star fas fa-star";
const starClassInactive = "rating__star far fa-star";
const starsLength = ratingStars.length;

let inputValue = ratingInput.value;

subscribeRating(ratingStars);
fillRating(ratingStars, inputValue);


function subscribeRating(stars) {
    let i;
    stars.map((star) => {
        star.onclick = () => {
            i = stars.indexOf(star);
            ratingInput.value = i;
            
            for (j = 0; j <= i; j++) stars[j].className = starClassActive;
            for (j = i + 1; j < starsLength; j++) stars[j].className = starClassInactive;
        };
    });
}

function fillRating(stars, i) {
    if (i > starsLength) i = starsLength - 1;
    if (i < 0) i = 0;
    
    for (j = 0; j <= i; j++) stars[j].className = starClassActive;
    for (j = i + 1; j < starsLength; j++) stars[j].className = starClassInactive;
}