﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using WebShop.Data.Enums;

namespace WebShop.Data.Models
{
    [JsonObject]
    public class Sock
    {
        [JsonProperty("id")] public int Id { get; set; }
        [JsonProperty("name")] public string Name { get; set; }
        [JsonProperty("price")] public int Price { get; set; }
        [JsonProperty("image")] public string Image { get; set; }
        [JsonProperty("description")] public string Description { get; set; }
        [JsonProperty("not_exists")] public bool NotExists { get; set; }
        [JsonProperty("sock_type"), JsonConverter(typeof(StringEnumConverter))] public SockType SockType { get; set; }
    }
}