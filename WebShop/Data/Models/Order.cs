﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace WebShop.Data.Models
{
    [JsonObject]
    public class Order
    {
        [JsonProperty("Дата заказа")] public string Date { get; set; }
        [JsonProperty("ФИО")] public string Name { get; set; }
        [JsonProperty("почтовый_индекс")] public int PostIndex { get; set; }
        [JsonProperty("адрес")] public string Address { get; set; }
        [JsonProperty("номер_телефона")] public string PhoneNumber { get; set; }
        [JsonProperty("общая стоимость")] public int TotalCost { get; set; }
        [JsonProperty("товары")] public List<SockInOrder> Socks { get; set; }
    }
}