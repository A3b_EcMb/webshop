﻿using Newtonsoft.Json;

namespace WebShop.Data.Models
{
    [JsonObject]
    public class News
    {
        [JsonProperty("id")] public int Id { get; set; }
        [JsonProperty("name")] public string Name { get; set; }
        [JsonProperty("text")] public string Text { get; set; }
        [JsonProperty("picture")] public string Picture { get; set; }
    }
}