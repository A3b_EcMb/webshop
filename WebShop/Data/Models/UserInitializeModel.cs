﻿using Newtonsoft.Json;

namespace WebShop.Data.Models
{
    [JsonObject]
    public class UserInitializeModel
    {
        [JsonProperty("email")] public string Email { get; set; }
        [JsonProperty("email_confirmed")] public bool EmailConfirmed { get; set; }
        [JsonProperty("password")] public string Password { get; set; }
        [JsonProperty("avatar")] public string Avatar { get; set; }
        [JsonProperty("site_name")] public string SiteName { get; set; }
        [JsonProperty("phone_number")] public string PhoneNumber { get; set; }
        [JsonProperty("roles")] public string[] Roles { get; set; }
    }
}