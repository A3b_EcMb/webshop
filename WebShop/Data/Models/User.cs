﻿using Microsoft.AspNetCore.Identity;

namespace WebShop.Data.Models
{
    public class User : IdentityUser
    {
        public string Avatar { get; set; }
        public string SiteName { get; set; }
    }
}