﻿using Newtonsoft.Json;

namespace WebShop.Data.Models
{
    [JsonObject]
    public class SockInCart
    {
        [JsonProperty("id")] public int Id { get; set; }
        [JsonProperty("count")] public int Count { get; set; }
        
        [JsonProperty("customer_id")] public string CustomerId { get; set; }
        
        [JsonProperty("sock_id")] public int SockId { get; set; }
    }
}