﻿using Newtonsoft.Json;

namespace WebShop.Data.Models
{
    [JsonObject]
    public class SockInOrder
    {
        [JsonProperty("id_стельки")] public int SockId { get; set; }
        [JsonProperty("количество")] public int Count { get; set; }
    }
}