﻿using System;
using System.Linq;
using Newtonsoft.Json;
using WebShop.Data.Contexts;
using WebShop.Data.Enums;
using WebShop.Models;

namespace WebShop.Data.Models
{
    [JsonObject]
    public class Review
    {
        [JsonProperty("id")] public int Id { get; set; }
        [JsonProperty("date")] public DateTime Date { get; set; }
        [JsonProperty("rating")] public int Rating { get; set; }
        [JsonProperty("text")] public string Text { get; set; }
        [JsonProperty("image")] public string Image { get; set; }
        [JsonProperty("author_id")] public string AuthorName { get; set; }
    }
}