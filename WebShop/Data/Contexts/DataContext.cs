﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using WebShop.Data.Models;

namespace WebShop.Data.Contexts
{
    public sealed class DataContext : IdentityDbContext<User>
    {
        public DbSet<SockInCart> SocksInCart { get; set; }

        public DataContext(DbContextOptions<DataContext> options) : base(options)
        {
        }
    }
}