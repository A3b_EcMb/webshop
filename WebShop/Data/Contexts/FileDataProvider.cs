﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using Newtonsoft.Json;
using WebShop.Data.Models;

namespace WebShop.Data.Contexts
{
    public class FileDataProvider
    {
        private const string SocksFilePath = "Files/socks.json";
        private const string ReviewsFilePath = "Files/reviews.json";
        private const string UsersFilePath = "Files/users.json";
        private const string NewsFilePath = "Files/news.json";
        private const string OrdersFilePath = "Files/orders.json";

        private static int _sockId = 1;
        private static int _reviewId = 1;
        private static int _newsId = 1;

        public static List<Sock> Socks { get; }
        public static List<Review> Reviews { get; }
        public static List<UserInitializeModel> UserInitializeModels { get; }
        public static List<News> News { get; }
        public static List<Order> Orders { get; }

        static FileDataProvider()
        {
            Socks = InitializeList<Sock>(SocksFilePath);
            Reviews = InitializeList<Review>(ReviewsFilePath);
            UserInitializeModels = InitializeList<UserInitializeModel>(UsersFilePath);
            News = InitializeList<News>(NewsFilePath);
            Orders = InitializeList<Order>(OrdersFilePath);

            if (Socks.Count > 0) _sockId = Socks.Max(x => x.Id);
            if (Reviews.Count > 0) _reviewId = Reviews.Max(x => x.Id);
            if (News.Count > 0) _newsId = News.Max(x => x.Id);
        }

        public static int GetSockId()
        {
            _sockId++;
            return _sockId;
        }

        public static int GetReviewId()
        {
            _reviewId++;
            return _reviewId;
        }

        public static int GetNewsId()
        {
            _newsId++;
            return _newsId;
        }

        public static void Save()
        {
            Save(Socks, SocksFilePath);
            Save(Reviews, ReviewsFilePath);
            Save(UserInitializeModels, UsersFilePath);
            Save(News, NewsFilePath);
            Save(Orders, OrdersFilePath);
        }

        private static void Save<T>(List<T> items, string path)
        {
            using var sw = new StreamWriter(path, false);
            string jsonString = JsonConvert.SerializeObject(items, Formatting.Indented);
            sw.Write(jsonString);
        }

        private static List<T> InitializeList<T>(string path)
        {
            var items = new List<T>();
            var models = LoadDataIfExists<List<T>>(path);
            if (models != null)
                items.AddRange(models);
            return items;
        }
        
        private static T LoadDataIfExists<T>(string path)
        {
            if (!File.Exists(path))
                return default;

            T result;
            using (var sr = new StreamReader(path))
            {
                string json = sr.ReadToEnd();
                result = JsonConvert.DeserializeObject<T>(json);
            }
            return result;
        }
    }
}