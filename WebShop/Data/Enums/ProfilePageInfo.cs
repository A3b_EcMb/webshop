﻿namespace WebShop.Data.Enums
{
    public enum ProfilePageInfo
    {
        Default = 0,
        EmailVerified = 1,
        EmailVerificationSent = 2,
        NeedToVerifyEmail = 3,
        PasswordChanged = 4,
    }
}