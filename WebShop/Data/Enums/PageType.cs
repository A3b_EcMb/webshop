﻿namespace WebShop.Data.Enums
{
    public enum PageType
    {
        Home = 0,
        Catalog = 1,
        Cart = 2,
        Reviews = 3,
    }
}