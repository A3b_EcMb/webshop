﻿using System.Runtime.Serialization;

namespace WebShop.Data.Enums
{
    public enum UserRole
    {
        [EnumMember(Value = "user")] User,
        [EnumMember(Value = "moderator")] Moderator,
        [EnumMember(Value = "admin")] Admin
    }
}