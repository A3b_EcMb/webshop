﻿using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace WebShop.Data.Enums
{
    public enum SockType
    {
        [Display(Name = "Меховая"), EnumMember(Value = "fur")] Fur = 0,
        [Display(Name = "Кожаная"), EnumMember(Value = "leather")] Leather = 1,
        [Display(Name = "Пенная"), EnumMember(Value = "foamy")] Foamy = 2,
        [Display(Name = "Пробковая"), EnumMember(Value = "cork")] Cork = 3,
        [Display(Name = "Ортопедическая"), EnumMember(Value = "orto")] Orto = 4
    }
}