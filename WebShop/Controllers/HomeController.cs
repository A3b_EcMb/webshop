﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using WebShop.Models;

namespace WebShop.Controllers
{
    public class HomeController : Controller
    {
        private HomeViewModel _model;

        public HomeController()
        {
            _model = new HomeViewModel
            {
                ImageLink = "../images/home.jpg",
                ImageCutLink = "../images/sockCut.jpg"
            };
        }

        public IActionResult Index()
        {
            return View(_model);
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}