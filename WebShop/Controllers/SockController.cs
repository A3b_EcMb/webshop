﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using WebShop.Data.Contexts;
using WebShop.Data.Enums;
using WebShop.Data.Models;
using WebShop.Models;

namespace WebShop.Controllers
{
    public class SockController : Controller
    {
        private readonly IWebHostEnvironment _webHostEnvironment;

        public SockController(IWebHostEnvironment webHostEnvironment)
        {
            _webHostEnvironment = webHostEnvironment;
        }

        [HttpGet]
        public IActionResult Index(int id, int fromPage)
        {
            var sock = FileDataProvider.Socks.FirstOrDefault(x => x.Id == id);
            if (sock == null)
                return RedirectToCatalog();

            var model = sock.ToViewModel((PageType) fromPage);
            return View(model);
        }

        [HttpGet]
        public async Task<IActionResult> GetSockModal(int id)
        {
            var sock = FileDataProvider.Socks.FirstOrDefault(x => x.Id == id);
            if (sock == null)
                return await Task.FromResult(PartialView("Modals/SockModalPartial", null));
            
            var model = sock.ToViewModel(PageType.Home);
            return await Task.FromResult(PartialView("Modals/SockModalPartial", model));
        }

        [HttpGet]
        public IActionResult GoBack(int fromPage)
        {
            var page = (PageType)fromPage;
            return page switch
            {
                PageType.Cart => RedirectToCart(),
                PageType.Catalog => RedirectToCatalog(),
                PageType.Home => RedirectToMainIndex(),
                _ => throw new ArgumentOutOfRangeException(nameof(fromPage), fromPage, null)
            };
        }
        
        [HttpGet]
        [Authorize(Roles = "admin")]
        public IActionResult AddOrEdit(int? id, int? fromPage)
        {
            var page = fromPage != null ? (PageType)fromPage.Value : PageType.Catalog;
            var sock = id != null ? FileDataProvider.Socks.FirstOrDefault(x => x.Id == id) : null;
            var model = sock != null ? sock.ToViewModel(page) : new SockViewModel { Id = -1, FromPage = 1 };

            if (sock != null)
            {
                string uploadFolder = Path.Combine(_webHostEnvironment.WebRootPath, "images/socks");
                
                string filePath = Path.Combine(uploadFolder, sock.Image);
                using var fs = System.IO.File.OpenRead(filePath);
                
                var file = new FormFile(fs, 0, fs.Length, null, Path.GetFileName(fs.Name))
                {
                    Headers = new HeaderDictionary(),
                    ContentType = "application/pdf"
                };
                model.UploadedImage = file;
            }

            // model.AvailableBrands = GetAvailableBrands();
            return View(model);
        }

        [HttpPost]
        [Authorize(Roles = "admin")]
        [ValidateAntiForgeryToken]
        public IActionResult AddOrEdit(SockViewModel model)
        {
            if (!ModelState.IsValid)
                return View(model);

            var sock = FileDataProvider.Socks.FirstOrDefault(x => x.Id == model.Id);
            if (sock == null)
            {
                if (model.UploadedImage == null)
                {
                    ModelState.AddModelError("UploadedImage", "Загрузите превью товара");
                    return View(model);
                }
                sock = new Sock
                {
                    Id = FileDataProvider.GetSockId()
                };
                if (!TrySetDataForSock(model, sock))
                    return View(model);
                
                FileDataProvider.Socks.Add(sock);
                FileDataProvider.Save();
            }
            else
            {
                if (!TrySetDataForSock(model, sock))
                    return View(model);

                FileDataProvider.Save();
            }

            return RedirectToIndex(model.Id);
        }

        [HttpGet]
        [Authorize(Roles = "admin")]
        public IActionResult Delete(int id, int fromPage)
        {
            var sock = FileDataProvider.Socks.FirstOrDefault(x => x.Id == id);
            if (sock != null)
            {
                FileDataProvider.Socks.Remove(sock);
                FileDataProvider.Save();
            }

            return GoBack(fromPage);
        }

        private IActionResult RedirectToCatalog()
        {
            return RedirectToAction("Index", "Catalog");
        }
        
        private IActionResult RedirectToCart()
        {
            return RedirectToAction("Index", "Cart");
        }

        private IActionResult RedirectToIndex(int id)
        {
            return RedirectToAction("Index", new { id });
        }

        private IActionResult RedirectToMainIndex()
        {
            return RedirectToAction("Index", "Home");
        }

        private bool TrySetDataForSock(SockViewModel model, Sock sock)
        {
            if (model.UploadedImage != null)
            {
                if (!TryProcessUploadedImage(model.UploadedImage, out string fileName))
                    return false;
                
                sock.Image = fileName;
            }

            sock.Name = model.Name;
            sock.Description = model.Description;
            sock.Price = model.Price;
            sock.SockType = model.SockType;
            sock.NotExists = model.NotExists;

            return true;
        }
        
        private bool TryProcessUploadedImage(IFormFile file, out string fileName)
        {
            fileName = null;
            if (file == null)
                return false;

            string uploadFolder = Path.Combine(_webHostEnvironment.WebRootPath, "images/socks");
            fileName = Guid.NewGuid() + "_" + file.FileName;
            string filePath = Path.Combine(uploadFolder, fileName);
            using var fileStream = new FileStream(filePath, FileMode.Create);
            file.CopyTo(fileStream);

            return true;
        }
    }
}