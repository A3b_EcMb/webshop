﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using WebShop.Data.Contexts;
using WebShop.Data.Enums;
using WebShop.Data.Models;
using WebShop.Models;

namespace WebShop.Controllers
{
    public class ReviewsController : Controller
    {
        private const int ReviewsOnIndexCount = 3;
        
        private DataContext _context;
        private UserManager<User> _userManager;

        public ReviewsController(DataContext context, UserManager<User> userManager)
        {
            _context = context;
            _userManager = userManager;
        }

        public async Task<IActionResult> Index()
        {
            string userName = "";
            var user = await _userManager.GetUserAsync(HttpContext.User);
            if (user != null)
                userName = user.UserName;
            
            var models = FileDataProvider.Reviews
                .Select(x => x.ToViewModel(_context, PageType.Reviews, userName))
                .ToList();
            
            return View(models);
        }

        [HttpGet]
        public async Task<IActionResult> GetReviewsIndex()
        {
            var random = new Random();
            var reviews = FileDataProvider.Reviews
                .OrderBy(x => random.Next(100))
                .Take(ReviewsOnIndexCount)
                .Select(x => x.ToViewModel(_context, PageType.Home))
                .ToList();

            return await Task.FromResult(PartialView("Reviews/ReviewsListPartial", reviews));
        }

        [HttpGet]
        public async Task<IActionResult> GetReviewModal(int id)
        {
            var review = FileDataProvider.Reviews.FirstOrDefault(x => x.Id == id);
            if (review == null)
                return await Task.FromResult(PartialView("Modals/ReviewModalPartial", null));
            
            var user = await _userManager.GetUserAsync(HttpContext.User);
            var model = review.ToViewModel(_context, PageType.Reviews, user?.UserName);
            return await Task.FromResult(PartialView("Modals/ReviewModalPartial", model));
        }

        [HttpGet]
        [Authorize(Roles = "user")]
        public async Task<IActionResult> AddOrEdit(int id, int? fromPage)
        {
            var page = fromPage != null ? (PageType)fromPage.Value : PageType.Reviews;
            var user = await _userManager.GetUserAsync(HttpContext.User);
            if (user == null)
                return RedirectToAction("Index");
            
            var review = FileDataProvider.Reviews.FirstOrDefault(x => x.Id == id);
            if (review == null)
                return View(new ReviewViewModel { Id = -1, FromPage = (int) PageType.Reviews });
            
            if (review.AuthorName != user.UserName)
                return RedirectToAction("Index");
            
            return View(review.ToViewModel(_context, page));
        }

        [HttpPost]
        [Authorize(Roles = "user")]
        public async Task<IActionResult> AddOrEdit(ReviewViewModel model)
        {
            var user = await _userManager.GetUserAsync(HttpContext.User);
            if (user == null)
                return RedirectToAction("Index");
            
            if (!ModelState.IsValid)
                return View(model);

            var review = FileDataProvider.Reviews.FirstOrDefault(x => x.Id == model.Id);
            if (review == null)
            {
                review = new Review
                {
                    Id = FileDataProvider.GetReviewId(),
                    Date = DateTime.Now,
                    AuthorName = user.SiteName,
                    Image = string.IsNullOrWhiteSpace(user.Avatar) ? "human_default.jpg" : user.Avatar
                };
                SetDataForReview(model, review);
                FileDataProvider.Reviews.Add(review);
                FileDataProvider.Save();
            }
            else
            {
                if (user.Id != review.AuthorName)
                    return RedirectToAction("Index");
                
                SetDataForReview(model, review);
                FileDataProvider.Save();
            }

            return RedirectToAction("AddOrEdit", new { id = review.Id });
        }

        [HttpPost]
        [Authorize(Roles = "admin")]
        public IActionResult Delete(int id)
        {
            var review = FileDataProvider.Reviews.FirstOrDefault(x => x.Id == id);
            if (review != null)
            {
                FileDataProvider.Reviews.Remove(review);
                FileDataProvider.Save();
            }

            return RedirectToAction("Index");
        }

        private void SetDataForReview(ReviewViewModel model, Review review)
        {
            review.Rating = model.Rating;
            review.Text = model.Text;
        }
    }
}