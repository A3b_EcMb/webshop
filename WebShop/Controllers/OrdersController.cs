﻿using System.IO;
using Microsoft.AspNetCore.Mvc;

namespace WebShop.Controllers
{
    public class OrdersController : Controller
    {
        private const string FilePath = "Files/orders.json";
        private const string FileType = "application/pdf";
        private const string FileName = "orders.json";
        
        public FileResult Index()
        {
            var fs = new FileStream(FilePath, FileMode.Open);
            return File(fs, FileType, FileName);
        }
    }
}