﻿using System;
using System.IO;
using System.Linq;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using WebShop.Data.Contexts;
using WebShop.Data.Models;
using WebShop.Models;

namespace WebShop.Controllers
{
    public class NewsController : Controller
    {
        private readonly IWebHostEnvironment _webHostEnvironment;

        public NewsController(IWebHostEnvironment webHostEnvironment)
        {
            _webHostEnvironment = webHostEnvironment;
        }

        [HttpGet]
        public IActionResult Index()
        {
            var models = FileDataProvider.News
                .Select(x => x.ToViewModel())
                .ToList();
            
            return View(models);
        }

        [HttpGet]
        [Authorize(Roles = "admin")]
        public IActionResult AddOrEdit(int? id)
        {
            if (!id.HasValue)
                return View(new NewsViewModel { Id = -1 });
            
            var news = FileDataProvider.News.FirstOrDefault(x => x.Id == id);
            if (news == null)
                return RedirectToAction("Index");

            return View(news.ToViewModel());
        }

        [HttpPost]
        [Authorize(Roles = "admin")]
        public IActionResult AddOrEdit(NewsViewModel model)
        {
            if (!ModelState.IsValid)
                return View(model);
            
            var news = FileDataProvider.News.FirstOrDefault(x => x.Id == model.Id);
            if (news == null)
            {
                news = new News();
                news.Id = FileDataProvider.GetNewsId();
                FileDataProvider.News.Add(news);
            }
            
            SetDataForNews(news, model);
            FileDataProvider.Save();
            return RedirectToAction("Index");
        }

        [HttpGet]
        [Authorize(Roles = "admin")]
        public IActionResult Delete(int id)
        {
            var news = FileDataProvider.News.FirstOrDefault(x => x.Id == id);
            if (news == null)
                return RedirectToAction("Index");

            FileDataProvider.News.Remove(news);
            FileDataProvider.Save();
            return RedirectToAction("Index");
        }

        private void SetDataForNews(News news, NewsViewModel model)
        {
            news.Name = model.Name;
            news.Text = model.Text;
            
            if (model.File == null) return;
            
            string uploadFolder = Path.Combine(_webHostEnvironment.WebRootPath, "images/news");
            string fileName = Guid.NewGuid() + "_" + model.File.FileName;
            string filePath = Path.Combine(uploadFolder, fileName);
            using var fileStream = new FileStream(filePath, FileMode.Create);
            model.File.CopyTo(fileStream);

            news.Picture = fileName;
        }
    }
}