﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using WebShop.Data.Contexts;
using WebShop.Data.Enums;
using WebShop.Data.Models;
using WebShop.Models;

namespace WebShop.Controllers
{
    public class CartController : Controller
    {
        private DataContext _context;
        private UserManager<User> _userManager;

        public CartController(DataContext context, UserManager<User> userManager)
        {
            _context = context;
            _userManager = userManager;
        }
        
        [HttpGet]
        public IActionResult Index()
        {
            return View();
        }
        
        [HttpGet]
        public async Task<IActionResult> GetSocks()
        {
            var user = await _userManager.GetUserAsync(HttpContext.User);
            if (user == null)
                return PartialView("Socks/SocksInCartsListPartial", new SocksListViewModel());
            
            var socks = FileDataProvider.Socks.ToList();
            var socksInCart = _context.SocksInCart
                .Where(x => x.CustomerId == user.Id);
            var viewModels = new List<SockViewModel>();
            foreach (var sockInCart in socksInCart)
            {
                var sock = socks.FirstOrDefault(x => x.Id == sockInCart.SockId);
                if (sock == null)
                    continue;

                var model = sock.ToViewModel(PageType.Cart);
                viewModels.Add(model);
            }

            int totalCost = viewModels.Sum(x => x.Price);
            return PartialView("Socks/SocksInCartsListPartial", new SocksListViewModel
            {
                Socks = viewModels,
                IsCart = true,
                TotalCost = totalCost
            });
        }

        [Authorize(Roles = "user")]
        [HttpGet]
        public async Task<IActionResult> GetCartInfoHome(int id, int? change)
        {
            return await CheckAndGetCartInfo(id, change, PageType.Home);
        }
        
        [Authorize(Roles = "user")]
        [HttpGet]
        public async Task<IActionResult> GetCartInfoCatalog(int id, int? change)
        {
            return await CheckAndGetCartInfo(id, change, PageType.Catalog);
        }
        
        [Authorize(Roles = "user")]
        [HttpGet]
        public async Task<IActionResult> GetCartInfoInCart(int id, int? change)
        {
            return await CheckAndGetCartInfo(id, change, PageType.Cart);
        }

        private async Task<IActionResult> CheckAndGetCartInfo(int id, int? change, PageType fromPage)
        {
            var model = new SockCartControlsModel
            {
                Id = id,
                FromPage = (int)fromPage
            };
            var user = await _userManager.GetUserAsync(HttpContext.User);
            if (user == null)
                return PartialView("Socks/SockCartControlsPartial", model.WithCount(-1));
            
            string userId = user.Id;
            var sockInCart = _context.SocksInCart.FirstOrDefault(x => x.SockId == id && x.CustomerId == userId);
            
            if (sockInCart == null)
            {
                if (change == null || change.Value <= 0)
                    return PartialView("Socks/SockCartControlsPartial", model.WithCount(0));
                
                var sock = FileDataProvider.Socks.FirstOrDefault(x => x.Id == id);
                if (sock == null || sock.NotExists)
                    return PartialView("Socks/SockCartControlsPartial", model.WithCount(0));

                _context.SocksInCart.Add(new SockInCart
                {
                    SockId = id,
                    CustomerId = userId,
                    Count = change.Value
                });
                await _context.SaveChangesAsync();
                return PartialView("Socks/SockCartControlsPartial", model.WithCount(change.Value));
            }

            if (change == null)
                return PartialView("Socks/SockCartControlsPartial", model.WithCount(sockInCart.Count));

            sockInCart.Count += change.Value;
            await _context.SaveChangesAsync();
            
            if (sockInCart.Count > 0)
                return PartialView("Socks/SockCartControlsPartial", model.WithCount(sockInCart.Count));
            
            _context.SocksInCart.Remove(sockInCart);
            await _context.SaveChangesAsync();
            return PartialView("Socks/SockCartControlsPartial", model.WithCount(0));
        }

        [HttpGet]
        public async Task<IActionResult> GetTotalCartInfo()
        {
            var user = await _userManager.GetUserAsync(HttpContext.User);
            if (user == null)
                return PartialView("CartInfoPartial", 0);

            var socksInCart = _context.SocksInCart
                .Where(x => x.CustomerId == user.Id);

            int totalCost = 0;
            foreach (var sockInCart in socksInCart)
            {
                var sock = FileDataProvider.Socks.FirstOrDefault(x => x.Id == sockInCart.SockId);
                if (sock == null)
                    continue;
                
                totalCost += sock.Price * sockInCart.Count;
            }
            
            return PartialView("CartInfoPartial", totalCost);
        }

        [Authorize(Roles = "user")]
        [HttpGet]
        public async Task<int> GetCartCount(int id)
        {
            var user = await _userManager.GetUserAsync(HttpContext.User);
            if (user == null)
                return 0;
            
            string userId = user.Id;
            var sockInCart = _context.SocksInCart.FirstOrDefault(x => x.SockId == id && x.CustomerId == userId);

            if (sockInCart == null) return 0;
            return sockInCart.Count;
        }

        [Authorize(Roles = "user")]
        [HttpGet]
        public async Task<int> GetCartTotalCount()
        {
            var user = await _userManager.GetUserAsync(HttpContext.User);
            if (user == null)
                return await Task.FromResult(0);
            
            string userId = user.Id;
            int count = 0;
            var socks = _context.SocksInCart.Where(x => x.CustomerId == userId);

            foreach (var sock in socks)
            {
                count += sock.Count;
            }
            
            return await Task.FromResult(count);
        }

        [Authorize(Roles = "user")]
        [HttpGet]
        public async Task<IActionResult> ClearCart()
        {
            var user = await _userManager.GetUserAsync(HttpContext.User);
            if (user == null)
                return RedirectToAction("Index");

            var socksInCarts = _context.SocksInCart
                .Where(x => x.CustomerId == user.Id);
            
            foreach (var sockInCart in socksInCarts)
            {
                _context.SocksInCart.Remove(sockInCart);
            }

            await _context.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        [Authorize(Roles = "user")]
        [HttpGet]
        public async Task<IActionResult> ConfirmOrder()
        {
            var user = await _userManager.GetUserAsync(HttpContext.User);
            if (user == null)
                return RedirectToAction("Index", "Home");
            
            var socksInCart = _context.SocksInCart
                .Where(x => x.CustomerId == user.Id);

            int totalCost = 0;
            foreach (var sockInCart in socksInCart)
            {
                var sock = FileDataProvider.Socks.FirstOrDefault(x => x.Id == sockInCart.SockId);
                if (sock == null)
                    continue;
                
                totalCost += sock.Price * sockInCart.Count;
            }

            var model = new ConfirmOrderViewModel
            {
                TotalCost = totalCost
            };
            return View(model);
        }

        [Authorize(Roles = "user")]
        [HttpPost]
        public async Task<IActionResult> ConfirmOrder(ConfirmOrderViewModel model)
        {
            var user = await _userManager.GetUserAsync(HttpContext.User);
            if (user == null)
                return RedirectToAction("Index", "Home");

            if (!ModelState.IsValid)
            {
                model.WasErrors = true;
                return View(model);
            }

            var socksInCart = _context.SocksInCart
                .Where(x => x.CustomerId == user.Id);

            var order = new Order
            {
                Date = DateTime.Now.ToLongDateString(),
                Name = model.Name,
                Address = model.Address,
                PhoneNumber = model.PhoneNumber,
                PostIndex = model.PostIndex,
                TotalCost = model.TotalCost,
                Socks = socksInCart.Select(x => new SockInOrder
                {
                    SockId = x.SockId,
                    Count = x.Count
                }).ToList()
            };
            FileDataProvider.Orders.Add(order);
            FileDataProvider.Save();
            
            _context.SocksInCart.RemoveRange(socksInCart);
            await _context.SaveChangesAsync();

            return View("OrderConfirmed", model);
        }
    }
}