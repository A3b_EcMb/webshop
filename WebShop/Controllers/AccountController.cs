﻿using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using WebShop.Data.Contexts;
using WebShop.Data.Enums;
using WebShop.Data.Models;
using WebShop.Models;

namespace WebShop.Controllers
{
    public class AccountController : Controller
    {
        private DataContext _context;
        private readonly UserManager<User> _userManager;
        private readonly SignInManager<User> _signInManager;
        private readonly IWebHostEnvironment _webHostEnvironment;
        private readonly EmailService _emailService;

        public AccountController(UserManager<User> userManager, SignInManager<User> signInManager,
            IWebHostEnvironment webHostEnvironment, DataContext context, EmailService emailService)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _webHostEnvironment = webHostEnvironment;
            _context = context;
            _emailService = emailService;
        }

        [HttpGet]
        public IActionResult Login(string returnUrl = null)
        {
            return View(new LoginViewModel { ReturnUrl = returnUrl });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(LoginViewModel model)
        {
            if (!ModelState.IsValid)
                return View(model);

            var result = await _signInManager.PasswordSignInAsync(model.Email, model.Password, model.Remember, false);
            if (!result.Succeeded)
            {
                ModelState.AddModelError("", "Неправильный логин и/или пароль");
                model.WasErrors = true;
                return View(model);
            }

            if (!string.IsNullOrWhiteSpace(model.ReturnUrl) && Url.IsLocalUrl(model.ReturnUrl))
                return Redirect(model.ReturnUrl);

            return RedirectToAction("Index", "Home");
        }

        [HttpGet, HttpPost]
        public IActionResult Logout(string returnUrl = null)
        {
            _signInManager.SignOutAsync();
            if (!string.IsNullOrWhiteSpace(returnUrl) && Url.IsLocalUrl(returnUrl))
                return Redirect(returnUrl);
            
            return RedirectToAction("Index", "Home");
        }

        [HttpGet]
        public IActionResult Register()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Register(RegisterViewModel model)
        {
            if (!ModelState.IsValid)
            {
                ModelState.AddModelError("Login", "Проверьте введённые данные");
                return View(model);
            }

            var user = new User
            {
                UserName = model.Email
            };

            bool wasErrors = false;
            if (UsersInitializer.HasUserLogin(model.Email))
            {
                ModelState.AddModelError(string.Empty, "Пользователь с таким логином уже существует!");
                wasErrors = true;
            }
            
            foreach (var validator in _userManager.PasswordValidators)
            {
                var passwordResult = await validator.ValidateAsync(_userManager, user, model.Password);
                if (passwordResult.Succeeded) continue;

                wasErrors = true;
                foreach (var error in passwordResult.Errors)
                {
                    ModelState.AddModelError(string.Empty, error.Description);
                }
            }
            foreach (var validator in _userManager.UserValidators)
            {
                var passwordResult = await validator.ValidateAsync(_userManager, user);
                if (passwordResult.Succeeded) continue;

                wasErrors = true;
                foreach (var error in passwordResult.Errors)
                {
                    ModelState.AddModelError(string.Empty, error.Description);
                }
            }

            if (wasErrors)
                return View(model);
            
            return RedirectToAction("EditProfileRegister", new
            {
                login = model.Email,
                password = model.Password,
                remember = model.Remember,
                returnUrl = model.ReturnUrl
            });
        }
        
        [HttpGet]
        public IActionResult EditProfileRegister(string login, string password, bool remember, string returnUrl)
        {
            return View(new RegisterWithProfileViewModel
            {
                Email = login,
                Password = password,
                PasswordConfirm = password,
                Remember = remember,
                ReturnUrl = returnUrl
            });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> EditProfileRegister(RegisterWithProfileViewModel model)
        {
            if (!ModelState.IsValid)
                return View(model);

            var user = new User
            {
                Email = model.Email,
                UserName = model.Email,
                PhoneNumber = model.PhoneNumber
            };

            var result = await _userManager.CreateAsync(user, model.Password);
            if (result.Succeeded)
            {
                await SendVerificationEmailMessageAsync(user);
                
                UsersInitializer.AddUser(model.Email, model.Password);
                await _userManager.AddToRoleAsync(user, "user");
                await _signInManager.PasswordSignInAsync(user, model.Password, model.Remember, false);
            }
            else
            {
                foreach (var error in result.Errors)
                {
                    ModelState.AddModelError(string.Empty, error.Description);
                }

                return View(model);
            }

            if (TryProcessUploadedAvatar(model.AvatarImage, out string fileName))
                user.Avatar = fileName;
            
            await _context.SaveChangesAsync();
            UsersInitializer.UpdateUserInfo(user);
            
            if (!string.IsNullOrWhiteSpace(model.ReturnUrl) && Url.IsLocalUrl(model.ReturnUrl))
                return Redirect(model.ReturnUrl);
            
            return View("RegisterSuccessful", model);
        }

        [HttpGet]
        [Authorize]
        public async Task<IActionResult> SendVerificationEmail()
        {
            var user = await _userManager.GetUserAsync(HttpContext.User);
            if (user == null)
                return RedirectToAction("Index", "Home");

            if (user.EmailConfirmed)
                return RedirectToAction("Profile");

            await SendVerificationEmailMessageAsync(user);
            return RedirectToAction("Profile", new { pageInfo = ProfilePageInfo.EmailVerificationSent });
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> ConfirmEmail(string userId, string token)
        {
            if (string.IsNullOrWhiteSpace(userId + token))
                return View("Error");

            var user = await _userManager.FindByIdAsync(userId);
            if (user == null)
                return View("Error");

            var result = await _userManager.ConfirmEmailAsync(user, token);
            if (!result.Succeeded)
                return View("Error");
            
            UsersInitializer.UpdateUserInfo(user);
            return RedirectToAction("Profile", new { pageInfo = ProfilePageInfo.EmailVerified });
        }

        [Authorize]
        [HttpGet]
        public async Task<IActionResult> Profile(ProfilePageInfo? pageInfo)
        {
            var user = await _userManager.GetUserAsync(HttpContext.User);
            if (user == null)
                return RedirectToAction("Index", "Home");
            
            var model = user.ToViewModel();
            if (user.EmailConfirmed)
                model.PageInfo = ProfilePageInfo.EmailVerified;
            else
                model.PageInfo = pageInfo ?? ProfilePageInfo.NeedToVerifyEmail;

            if (pageInfo is ProfilePageInfo.PasswordChanged)
                model.PageInfo = ProfilePageInfo.PasswordChanged;

            return View(model);
        }
        
        [HttpGet]
        public async Task<IActionResult> EditProfile()
        {
            var user = await _userManager.GetUserAsync(HttpContext.User);
            if (user == null)
                return RedirectToAction("Index", "Home");

            return View(user.ToViewModel());
        }

        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> EditProfile(ProfileViewModel model)
        {
            if (!ModelState.IsValid)
                return View(model);
            
            var user = await _userManager.GetUserAsync(HttpContext.User);
            if (user == null)
                return RedirectToAction("Index", "Home");
            
            if (user.Email != model.Email)
                return RedirectToAction("Index", "Home");

            if (user.Email != model.NewEmail && !string.IsNullOrWhiteSpace(model.NewEmail))
            {
                user.Email = model.NewEmail;
                user.UserName = model.NewEmail;
                user.EmailConfirmed = false;
                await SendVerificationEmailMessageAsync(user);
            }

            user.PhoneNumber = model.PhoneNumber;
            if (TryProcessUploadedAvatar(model.AvatarImage, out string fileName))
                user.Avatar = fileName;

            await _context.SaveChangesAsync();
            UsersInitializer.UpdateUserInfo(user);
            return RedirectToAction("Profile");
        }

        [Authorize]
        [HttpGet]
        public async Task<IActionResult> ChangePassword()
        {
            var user = await _userManager.GetUserAsync(HttpContext.User);
            if (user == null)
                return RedirectToAction("Index", "Home");

            var userModel = FileDataProvider.UserInitializeModels.FirstOrDefault(x => x.Email == user.Email);
            if (userModel == null)
                return RedirectToAction("Index", "Home");
            
            var model = new ChangePasswordViewModel
            {
                OldPasswordCheck = userModel.Password
            };
            return View(model);
        }

        [Authorize]
        [HttpPost]
        public async Task<IActionResult> ChangePassword(ChangePasswordViewModel model)
        {
            var user = await _userManager.GetUserAsync(HttpContext.User);
            if (user == null)
                return RedirectToAction("Index", "Home");

            var userModel = FileDataProvider.UserInitializeModels.FirstOrDefault(x => x.Email == user.Email);
            if (userModel == null)
                return RedirectToAction("Index", "Home");

            if (userModel.Password != model.OldPassword)
            {
                ModelState.AddModelError("OldPassword", "Неправильный текущий пароль");
                return View(model);
            }

            var result = await _userManager.ChangePasswordAsync(user, model.OldPassword, model.NewPassword);
            if (!result.Succeeded)
            {
                foreach (var error in result.Errors)
                {
                    ModelState.AddModelError(error.Code, error.Description);
                }

                return View(model);
            }

            userModel.Password = model.NewPassword;
            FileDataProvider.Save();
            await SendPasswordChangedEmailMessageAsync(userModel);

            return RedirectToAction("Profile");
        }

        [Authorize]
        [HttpGet, HttpPost]
        public async Task<IActionResult> Delete()
        {
            var user = await _userManager.GetUserAsync(HttpContext.User);
            if (user == null)
                return RedirectToAction("Index", "Home");

            if (await _userManager.IsInRoleAsync(user, "admin"))
                return RedirectToAction("Index", "Home");

            var result = await _userManager.DeleteAsync(user);
            if (!result.Succeeded)
                return RedirectToAction("Profile");
            
            UsersInitializer.DeleteUser(user.UserName);
            await _signInManager.SignOutAsync();
            return RedirectToAction("Index", "Home");
        }

        private bool TryProcessUploadedAvatar(IFormFile file, out string fileName)
        {
            fileName = null;
            if (file == null)
                return false;

            string uploadFolder = Path.Combine(_webHostEnvironment.WebRootPath, "images/avatars");
            fileName = Guid.NewGuid() + "_" + file.FileName;
            string filePath = Path.Combine(uploadFolder, fileName);
            using var fileStream = new FileStream(filePath, FileMode.Create);
            file.CopyTo(fileStream);

            return true;
        }

        private async Task SendVerificationEmailMessageAsync(User user)
        {
            string token = await _userManager.GenerateEmailConfirmationTokenAsync(user);
            string callbackUrl = Url.Action("ConfirmEmail", "Account", new { userId = user.Id, token = token },
                protocol: HttpContext.Request.Scheme);

            await _emailService.SendEmailAsync(user.Email, "Подтвердите ваш email",
                $"Подтвердите регистрацию, пройдя по ссылке: <a href='{callbackUrl}'>link</a>");
        }
        
        private async Task SendPasswordChangedEmailMessageAsync(UserInitializeModel user)
        {
            await _emailService.SendEmailAsync(user.Email, "Ваш пароль изменён",
                $"Ваш пароль на сайте был изменён. Новый пароль: {user.Password}");
        }
    }
}