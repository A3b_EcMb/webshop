﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WebShop.Data.Contexts;
using WebShop.Data.Enums;
using WebShop.Models;

namespace WebShop.Controllers
{
    public class CatalogController : Controller
    {
        private const int SocksOnIndexCount = 3;
        
        [HttpGet]
        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public IActionResult GetSocks()
        {
            var socks = FileDataProvider.Socks
                .OrderBy(x => x.NotExists)
                .Select(x => x.ToViewModel(PageType.Catalog))
                .ToList();
            
            return PartialView("Socks/SocksListPartialNew", new SocksListViewModel
            {
                Socks = socks
            });
        }

        public async Task<IActionResult> GetSocksCart()
        {
            var socks = FileDataProvider.Socks
                .OrderBy(x => x.NotExists)
                .Select(x => x.ToViewModel(PageType.Catalog))
                .ToList();
            
            return await Task.FromResult(PartialView("Socks/SocksInCartsListPartial", new SocksListViewModel
            {
                Socks = socks
            }));
        }

        [HttpGet]
        public async Task<IActionResult> GetSocksIndex()
        {
            var random = new Random();
            var socks = FileDataProvider.Socks
                .OrderBy(x => x.NotExists)
                .ThenBy(x => random.Next(100))
                .Take(SocksOnIndexCount)
                .Select(x => x.ToViewModel(PageType.Home))
                .ToList();
            
            return await Task.FromResult(PartialView("Socks/SocksListPartialNew", new SocksListViewModel
            {
                Socks = socks
            }));
        }
    }
}