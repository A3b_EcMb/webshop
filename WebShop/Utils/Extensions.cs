﻿using System.Linq;
using WebShop.Data.Contexts;
using WebShop.Data.Enums;
using WebShop.Data.Models;
using WebShop.Models;

namespace WebShop
{
    public static class Extensions
    {
        public static SockViewModel ToViewModel(this Sock sock, PageType fromPage)
        {
            return new SockViewModel
            {
                FromPage = (int) fromPage,
                Id = sock.Id,
                Name = sock.Name,
                Price = sock.Price,
                ImageSrc = $"../images/socks/{sock.Image}",
                Description = sock.Description,
                SockType = sock.SockType,
                NotExists = sock.NotExists
            };
        }

        public static ReviewViewModel ToViewModel(this Review review, DataContext context, PageType fromPage,
            string currentUserName = null)
        {
            var author = context.Users.FirstOrDefault(x => x.UserName == review.AuthorName);
            
            return new ReviewViewModel
            {
                Id = review.Id,
                Date = review.Date,
                Rating = review.Rating,
                Text = review.Text,
                AuthorName = author?.SiteName,
                IsAuthor = review.AuthorName == currentUserName,
                FromPage = (int) fromPage,
                ImageSrc = $"../images/avatars/{review.Image}"
            };
        }

        public static ProfileViewModel ToViewModel(this User user)
        {
            if (user == null)
            {
                return new ProfileViewModel
                {
                    Avatar = "../images/avatars/human_default.jpg"
                };
            }
            
            string avatar = string.IsNullOrWhiteSpace(user.Avatar) ? "human_default.jpg" : user.Avatar;
            return new ProfileViewModel
            {
                Email = user.UserName,
                SiteName = user.SiteName,
                Avatar = $"../images/avatars/{avatar}",
                PhoneNumber = user.PhoneNumber
            };
        }

        public static NewsViewModel ToViewModel(this News news)
        {
            return new NewsViewModel
            {
                Id = news.Id,
                Name = news.Name,
                Text = news.Text,
                Photo = $"../images/news/{news.Picture}"
            };
        }
    }
}