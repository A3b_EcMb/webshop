﻿namespace WebShop
{
    public static class LocalizedIdentityErrorMessages
    {
        public const string DuplicateEmail = "Почта '{0}' уже зарегистрирована";
        public const string DuplicateUserName = "Пользователь '{0}' уже зарегистрирован";
        public const string InvalidEmail = "Почта '{0}' введена некорректно";
        public const string DuplicateRoleName = "Роль '{0}' уже существует";
        public const string InvalidRoleName = "Роль '{0}' введена некорректно";
        public const string InvalidToken = "Токен недействителен";
        public const string InvalidUserName = "Имя пользователя '{0}' введено некорректно";
        public const string LoginAlreadyAssociated = "Логин '{0}' уже прикреплён к аккаунту";
        public const string PasswordMismatch = "Пароли не совпадают";
        public const string PasswordRequiresDigit = "В пароле должны использоваться цифры";
        public const string PasswordRequiresLower = "В пароле должны использоваться строчные буквы";
        public const string PasswordRequiresNonAlphanumeric = @"В пароле должны использоваться специальные символы (!\”$%&’()+,-.:;<=>?@[]_{|}§)";
        public const string PasswordRequiresUniqueChars = "В пароле должно быть как минимум {0} уникальных символов";
        public const string PasswordRequiresUpper = "В пароле должны использоваться заглавные буквы";
        public const string PasswordTooShort = "Минимальное количество символов пароля должно быть не менее 10";
        public const string UserAlreadyHasPassword = "Пароль совпадает с предыдущим";
        public const string UserAlreadyInRole = "Пользователь уже имеет эту роль";
        public const string UserNotInRole = "Пользователь не имеет эту роль";
        public const string UserLockoutNotEnabled = "Пользователь не заблокирован";
        public const string RecoveryCodeRedemptionFailed = "Неправильный код восстановления";
        public const string ConcurrencyFailure = "Системная ошибка";
        public const string DefaultIdentityError = "Системная ошибка";
    }
}