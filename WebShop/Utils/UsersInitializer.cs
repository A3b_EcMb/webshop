﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using WebShop.Data.Contexts;
using WebShop.Data.Models;

namespace WebShop
{
    public static class UsersInitializer
    {
        public static async Task InitializeAsync(UserManager<User> userManager, RoleManager<IdentityRole> roleManager, DataContext context)
        {
            if (await roleManager.FindByNameAsync("admin") == null)
                await roleManager.CreateAsync(new IdentityRole("admin"));
            
            if (await roleManager.FindByNameAsync("user") == null)
                await roleManager.CreateAsync(new IdentityRole("user"));
            
            var users = FileDataProvider.UserInitializeModels;
            foreach (var userModel in users)
            {
                if (await userManager.FindByNameAsync(userModel.Email) != null)
                    continue;
                
                var user = new User
                {
                    UserName = userModel.Email,
                    Email = userModel.Email,
                    EmailConfirmed = userModel.EmailConfirmed
                };
                var result = await userManager.CreateAsync(user, userModel.Password);

                if (!result.Succeeded)
                    continue;
                
                await userManager.AddToRolesAsync(user, userModel.Roles);

                user.Avatar = userModel.Avatar;
                user.PhoneNumber = userModel.PhoneNumber;
                user.SiteName = userModel.SiteName;
                await context.SaveChangesAsync();
            }
        }

        public static bool HasUserLogin(string login)
        {
            return FileDataProvider.UserInitializeModels.Any(x => x.Email == login);
        }

        public static void AddUser(string email, string password)
        {
            FileDataProvider.UserInitializeModels.Add(new UserInitializeModel
            {
                Email = email,
                Password = password,
                EmailConfirmed = false,
                Roles = new [] { "user" }
            });
            FileDataProvider.Save();
        }

        public static void UpdateUserInfo(User user)
        {
            var userModel = FileDataProvider.UserInitializeModels.FirstOrDefault(x => x.Email == user.UserName);
            if (userModel == null)
                return;

            userModel.Avatar = user.Avatar;
            userModel.PhoneNumber = user.PhoneNumber;
            userModel.SiteName = user.SiteName;
            userModel.Email = user.Email;
            userModel.EmailConfirmed = user.EmailConfirmed;
            FileDataProvider.Save();
        }

        public static void DeleteUser(string login)
        {
            var userModel = FileDataProvider.UserInitializeModels.FirstOrDefault(x => x.Email == login);
            if (userModel == null)
                return;

            FileDataProvider.UserInitializeModels.Remove(userModel);
            FileDataProvider.Save();
        }
    }
}