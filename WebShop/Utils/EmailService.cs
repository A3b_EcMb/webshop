﻿using System.Threading.Tasks;
using MailKit.Net.Smtp;
using MimeKit;

namespace WebShop
{
    public class EmailService
    {
        private const string SMTPHost = "smtp.gmail.com";
        private const int SMTPPort = 465;
        private const string SMTPEmail = "beautifulsocksmarket@gmail.com";
        private const string SMTPPassword = "gbdxxlyvxearjmmj"; //"itgbjavzyudqjglx";
        
        public async Task SendEmailAsync(string email, string subject, string message)
        {
            var emailMessage = new MimeMessage();
            
            emailMessage.From.Add(new MailboxAddress("Администрация сайта", SMTPEmail));
            emailMessage.To.Add(new MailboxAddress("", email));
            emailMessage.Subject = subject;
            emailMessage.Body = new TextPart(MimeKit.Text.TextFormat.Html)
            {
                Text = message
            };
            
            using (var client = new SmtpClient())
            {
                await client.ConnectAsync(SMTPHost, SMTPPort, true);
                await client.AuthenticateAsync(SMTPEmail, SMTPPassword);
                await client.SendAsync(emailMessage);
                await client.DisconnectAsync(true);
            }
        }
    }
}