﻿using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using WebShop.Data.Models;

namespace WebShop
{
    public class CustomPasswordValidator : IPasswordValidator<User>
    {
        private class PatternContainer
        {
            public string Pattern;
            public IdentityError Error;
        }
        
        private const int RequiredLength = 10;

        private static List<PatternContainer> _patterns = new List<PatternContainer>
        {
            new PatternContainer
            {
                Pattern = @"[a-z]",
                Error = new IdentityError { Description = "В пароле должны использоваться строчные буквы" }
            },
            new PatternContainer
            {
                Pattern = @"[A-Z]",
                Error = new IdentityError { Description = "В пароле должны использоваться заглавные буквы" }
            },
            new PatternContainer
            {
                Pattern = @"[0-9]",
                Error = new IdentityError { Description = "В пароле должны использоваться цифры" }
            },
            new PatternContainer
            {
                Pattern = @"[!@#$%^&*\(\)_\+\-\={}<>,\.\|""'~`:;\\?\/\[\] ]]",
                Error = new IdentityError { Description = @"В пароле должны использоваться специальные символы (!\”$%&’()+,-.:;<=>?@[]_{|}§)" }
            },
        };

        private IdentityError _defaultError = new IdentityError { Description = "Требования к паролю:" };
        private IdentityError _lengthError = new IdentityError
            { Description = $"Минимальное количество символов - {RequiredLength}" };

        public Task<IdentityResult> ValidateAsync(UserManager<User> manager, User user, string password)
        {
            bool hasErrors = false;
            var errors = new List<IdentityError>
            {
                _defaultError
            };
            if (string.IsNullOrWhiteSpace(password) || password.Length < RequiredLength)
            {
                errors.Add(_lengthError);
                hasErrors = true;
            }
            else
            {
                foreach (var pattern in _patterns)
                {
                    if (Regex.IsMatch(password, pattern.Pattern)) continue;
                    
                    errors.Add(pattern.Error);
                }
            }

            return Task.FromResult(hasErrors
                ? IdentityResult.Failed(errors.ToArray())
                : IdentityResult.Success);
        }
    }
}