﻿using System.ComponentModel.DataAnnotations;

namespace WebShop.Models
{
    public class LoginViewModel
    {
        [Required(ErrorMessage = "Введите email")]
        [Display(Name = "Email")]
        public string Email { get; set; }
        
        [Required(ErrorMessage = "Введите пароль")]
        [DataType(DataType.Password)]
        [Display(Name = "Пароль")]
        public string Password { get; set; }
        
        [Display(Name = "Запомнить")]
        public bool Remember { get; set; }
        
        public string ReturnUrl { get; set; }
        public bool WasErrors { get; set; }
    }
}