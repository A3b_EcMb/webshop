﻿using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Http;

namespace WebShop.Models
{
    public class NewsViewModel
    {
        public int Id { get; set; }
        
        [Display(Name = "Название поста")]
        [Required(ErrorMessage = "Не указано название поста")]
        public string Name { get; set; }
        
        [Display(Name = "Текст поста")]
        [Required(ErrorMessage = "Не указан текст поста")]
        public string Text { get; set; }
        
        [Display(Name = "Изображение поста")]
        public IFormFile File { get; set; }
        
        public string Photo { get; set; }
    }
}