﻿using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Http;
using WebShop.Data.Enums;

namespace WebShop.Models
{
    public class ProfileViewModel
    {
        public string Email { get; set; }
        public string Avatar { get; set; }
        
        [Display(Name = "Почта")]
        [DataType(DataType.EmailAddress, ErrorMessage = "Введите корректный email-адрес")]
        public string NewEmail { get; set; }
    
        [Required(ErrorMessage = "Введите ваше имя")]
        [Display(Name = "Полное имя покупателя")]
        public string CustomerName { get; set; }
    
        [Required(ErrorMessage = "Введите имя, которое будет отображаться на сайте")]
        [Display(Name = "Имя, которое будет отображаться на сайте")]
        public string SiteName { get; set; }

        [Required(ErrorMessage = "Введите ваш номер телефона")]
        [DataType(DataType.PhoneNumber, ErrorMessage = "Введите корректный номер телефона")]
        [Display(Name = "Номер телефона покупателя")]
        public string PhoneNumber { get; set; }

        [Required(ErrorMessage = "Загрузите аватар")]
        [DataType(DataType.Upload)]
        [Display(Name = "Аватар")]
        public IFormFile AvatarImage { get; set; }
        
        public ProfilePageInfo PageInfo { get; set; }
    }
}