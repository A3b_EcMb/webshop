﻿using System.ComponentModel.DataAnnotations;

namespace WebShop.Models
{
    public class ConfirmOrderViewModel
    {
        public int TotalCost { get; set; }
    
        [Required(ErrorMessage = "Не указано имя покупателя")]
        [Display(Name = "ФИО получателя")]
        public string Name { get; set; }
        
        [Required(ErrorMessage = "Не указан почтовый индекс")]
        [Display(Name = "Почтовый индекс получателя")]
        public int PostIndex { get; set; }
    
        [Required(ErrorMessage = "Не указан адрес доставки")]
        [Display(Name = "Адрес доставки (город, улица, дом, квартира)")]
        public string Address { get; set; }
    
        [Required(ErrorMessage = "Не указан номер телефона")]
        [DataType(DataType.PhoneNumber)]
        [Display(Name = "Номер телефона получателя")]
        public string PhoneNumber { get; set; }
    
        public bool WasErrors { get; set; }
    }
}