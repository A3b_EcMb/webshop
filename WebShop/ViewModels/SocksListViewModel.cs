﻿using System.Collections.Generic;

namespace WebShop.Models
{
    public class SocksListViewModel
    {
        public List<SockViewModel> Socks;
        public int TotalCost;
        public bool IsCart;
    }
}