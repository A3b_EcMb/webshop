﻿using WebShop.Data.Models;

namespace WebShop.Models
{
    public class SockInCartViewModel
    {
        public Sock Sock;
        public int Count;
        public int Cost;
    }
}