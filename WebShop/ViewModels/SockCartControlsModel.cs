﻿namespace WebShop.Models
{
    public class SockCartControlsModel
    {
        public int Count { get; set; }
        public bool IsCart { get; set; }
        public int Id { get; set; }
        public int FromPage { get; set; }

        public SockCartControlsModel WithCount(int count)
        {
            Count = count;
            return this;
        }
    }
}