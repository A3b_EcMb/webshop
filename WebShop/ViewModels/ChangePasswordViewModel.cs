﻿using System.ComponentModel.DataAnnotations;

namespace WebShop.Models
{
    public class ChangePasswordViewModel
    {
        public string OldPasswordCheck { get; set; }
        
        [Required(ErrorMessage = "Не указан текущий пароль")]
        [Compare("OldPasswordCheck", ErrorMessage = "Неверный пароль")]
        [DataType(DataType.Password)]
        public string OldPassword { get; set; }
        
        [Required(ErrorMessage = "Не указан новый пароль")]
        [DataType(DataType.Password)]
        [Display(Name = "Новый пароль")]
        public string NewPassword { get; set; }
        
        [Required(ErrorMessage = "Пароль не подтверждён")]
        [Compare("NewPassword", ErrorMessage = "Пароли не совпадают")]
        [DataType(DataType.Password)]
        [Display(Name = "Подтвердите пароль")]
        public string NewPasswordConfirm { get; set; }
    }
}