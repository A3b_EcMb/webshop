﻿using System;
using System.ComponentModel.DataAnnotations;

namespace WebShop.Models
{
    public class ReviewViewModel
    {
        public int Id { get; set; }
        public DateTime Date { get; set; }
        public int FromPage { get; set; }
        
        [Required(ErrorMessage = "Укажите рейтинг")]
        [Range(1, 5)]
        [Display(Name = "Рейтинг")]
        public int Rating { get; set; }
        
        [Required(ErrorMessage = "Введите текст отзыва")]
        [Display(Name = "Текст отзыва")]
        public string Text { get; set; }
        public string AuthorName { get; set; }
        public bool IsAuthor { get; set; }
        public string ImageSrc { get; set; }
    }
}