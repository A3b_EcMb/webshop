﻿using System.ComponentModel.DataAnnotations;

namespace WebShop.Models
{
    public class RegisterViewModel
    {
        [Required(ErrorMessage = "Не указан email")]
        [Display(Name = "Email")]
        public string Email { get; set; }
        
        [Required(ErrorMessage = "Не указан пароль")]
        [DataType(DataType.Password)]
        [Display(Name = "Пароль")]
        public string Password { get; set; }
        
        [Required(ErrorMessage = "Пароль не подтверждён")]
        [Compare("Password", ErrorMessage = "Пароли не совпадают")]
        [DataType(DataType.Password)]
        [Display(Name = "Подтвердите пароль")]
        public string PasswordConfirm { get; set; }
        
        [Display(Name = "Запомнить")]
        public bool Remember { get; set; }
        
        public string ReturnUrl { get; set; }
    }
}