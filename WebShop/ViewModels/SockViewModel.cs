﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Rendering;
using WebShop.Data.Enums;
using WebShop.Data.Models;

namespace WebShop.Models
{
    public class SockViewModel
    {
        public int Id { get; set; }
        public int FromPage { get; set; }
        
        [Required(ErrorMessage = "Введите название товара")]
        [Display(Name = "Название")]
        public string Name { get; set; }
        
        [Required(ErrorMessage = "Введите цену товара")]
        [Display(Name = "Стоимость")]
        public int Price { get; set; }
        
        [Display(Name = "Описание")]
        public string Description { get; set; }
        
        [Display(Name = "Товара нет в наличии")]
        public bool NotExists { get; set; }

        [Required(ErrorMessage = "Укажите тип стелек")]
        [Display(Name = "Тип")]
        public SockType SockType { get; set; }

        public string ImageSrc { get; set; }
        
        [Display(Name = "Превью товара")]
        public IFormFile UploadedImage { get; set; }
    }
}