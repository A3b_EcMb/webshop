﻿using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Http;

namespace WebShop.Models
{
    public class RegisterWithProfileViewModel : RegisterViewModel
    {
        public string Avatar { get; set; }
    
        [Required(ErrorMessage = "Введите ваше имя")]
        [Display(Name = "Полное имя покупателя")]
        public string CustomerName { get; set; }
    
        [Required(ErrorMessage = "Введите имя, которое будет отображаться на сайте")]
        [Display(Name = "Имя, которое будет отображаться на сайте")]
        public string SiteName { get; set; }
    
        [Required(ErrorMessage = "Введите ваш номер телефона")]
        [DataType(DataType.PhoneNumber, ErrorMessage = "Введите корректный номер")]
        [Display(Name = "Номер телефона покупателя")]
        public string PhoneNumber { get; set; }

        [Required(ErrorMessage = "Загрузите аватар")]
        [DataType(DataType.Upload)]
        [Display(Name = "Аватар")]
        public IFormFile AvatarImage { get; set; }
    }
}